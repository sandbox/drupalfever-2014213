<?php

/**
 * @file
 * Page/form showing image styles in use.
 */

/**
 * Form for uploading and displaying an image using selected style.
 */
function dfslideshowfeature_style_form($form, &$form_state) {
  
  // Add the CSS for this feature.
  drupal_add_css (drupal_get_path('module', 'dfslideshowfeature') . "/css/dfslideshowpage.css");
  
  $image_fid = variable_get('dfslideshowfeature_image_fid', FALSE);
  if ($image_fid == FALSE) {
    
    global $user;
    
    $filepath = drupal_get_path('module', 'dfslideshowfeature') . "/images/slide-sample.jpg";
    
    $image = dfcopy_file($filepath, 'public://dfslideshowfeature_images');
    variable_set('dfslideshowfeature_image_fid', $image->fid);
  }
  else {
    
    $image = file_load($image_fid);
  }
  
  $style = variable_get('dfslideshowfeature_style_name', 'df-banner-image');
  
  //If style is set as overlay, make sure that the overlay file has been copied to its appropriate place.
  if ($style == "dfslideshowfeature_overlay") {
    
    $destination = 'public://dfslideshowfeature_images/banner-overlay.png';
    $exist = file_destination($destination, FILE_EXISTS_ERROR);
    if (!$exist) {
      
      $source = drupal_get_path('module', 'dfslideshowfeature') . "/images/banner-overlay.png";
      dfcopy_file($source, $destination);
    }
  }
  
  $image_fid = variable_get('dfslideshowfeature_logo_fid', FALSE);
  if ($image_fid != FALSE) {
    
    $logo = file_load($image_fid);
  }
  else {
    
    $logo = '';
  }
  
  $form['image'] = array(
    '#markup'        => theme('dfslideshowfeature_image', array('image' => $image, 'logo' => $logo, 'style' => $style)),
  );
  
  $form['dfslideshowfeature_overlay'] = array(
    '#type'            => 'checkbox',
    '#title'           => t('Enable overlay'),
    '#default_value'   => variable_get('dfslideshowfeature_overlay', FALSE),
  );
  
  $form['dfslideshowfeature_logo_fid'] = array(
    '#title'           => t('Image'),
    '#type'            => 'managed_file',
    '#description'     => t('The uploaded image will be displayed on this page using the image style choosen below.'),
    '#default_value'   => variable_get('dfslideshowfeature_logo_fid', ''),
    '#upload_location' => 'public://dfslideshow_images/',
  );
  
  $form['dfslideshowfeature_style_name'] = array(
    '#title'           => t('Image style'),
    '#type'            => 'select',
    '#description'     => t('Choose an image style to use when displaying this image.'),
    '#options'         => array(
      'df-banner-image'  => 'df-banner-image',
      'dfslideshow_overlay'  => 'dfslideshow_overlay',
    ),//image_style_options(TRUE),
    '#default_value'   => variable_get('dfslideshowfeature_style_name', 'df-banner-image'),
  );
  
  $form['submit'] = array(
    '#type'            => 'submit',
    '#value'           => t('Save'),
  );
  
  return $form;
}

/**
 * Verifies that the user supplied an image with the form.
 */
function dfslideshowfeature_style_form_validate($form, &$form_state) {
  
  if ((!isset($form_state['values']['dfslideshowfeature_logo_fid']))
  ||  (!is_numeric($form_state['values']['dfslideshowfeature_logo_fid']))) {
    
    form_set_error('dfslideshowfeature_image_fid', t('Please, browse for a logo to upload.'));
  }
}

/**
 * Form Builder; Display a form for uploading an image.
 */
function dfslideshowfeature_style_form_submit($form, &$form_state) {
  
  if ($form_state['values']['dfslideshowfeature_logo_fid'] != 0) {
    
    $file = file_load($form_state['values']['dfslideshowfeature_logo_fid']);
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    file_usage_add($file, 'dfslideshowfeature', 'sample_image', 1);
    variable_set('dfslideshowfeature_logo_fid', $file->fid);
    drupal_set_message(t('The image @image_name was uploaded and saved with an ID of @fid and will be displayed using the style @style.', array('@image_name' => $file->filename, '@fid' => $file->fid, '@style' => $form_state['values']['dfslideshowfeature_style_name'])));
  }
  else if ($form_state['values']['dfslideshowfeature_logo_fid'] == 0) {
    
    $fid = variable_get('dfslideshowfeature_logo_fid', FALSE);
    $file = $fid ? file_load($fid) : FALSE;
    
    if ($file) {
      
      drupal_set_message(t('The image @image_name was removed.', array('@image_name' => $file->filename)));
      file_usage_delete($file, 'dfslideshowfeature', 'sample_image', 1);
      file_delete($file);
    }
    
    variable_set('dfslideshowfeature_logo_fid', FALSE);
  }
  
  variable_set('dfslideshowfeature_style_name', $form_state['values']['dfslideshowfeature_style_name']);
  variable_set('dfslideshowfeature_overlay', $form_state['values']['dfslideshowfeature_overlay']);
}

/**
 * @desc This function will copy a file without the need of creating a file object first.
 * @param $source (String) Required -> The original path of the file. Ex. $source = drupal_get_path('module', 'dfslideshowfeature') . "/images/slide-sample.jpg";
 * @param $destination (String) Required -> The path of the destination. It can be the path of the destination folder only or it may have the name of the file as well, which could be different than original file. Ex. $destination = "public://dfslideshowfeature_images";
 * @param $replace (Integer) Optional -> A flag that is set as default to FILE_EXISTS_REPLACE. The two other options are FILE_EXISTS_RENAME and FILE_EXISTS_ERROR.
 * @return (stdClass) File object if the copy is successful, or FALSE in the event of an error.
 */
function dfcopy_file($source, $destination, $replace = FILE_EXISTS_REPLACE) {
  
  global $user;
  
  $file = new stdClass();
  $file->filename = basename($source);
  $file->filepath = $source;
  $file->filemime = file_get_mimetype($source);
  $file->filesize = filesize($source);
  $file->uid = $user->uid;
  $file->status = FILE_STATUS_PERMANENT;
  $file->uri = $source;
  $file->display = 1;
  $file->description = '';
  
  return file_copy($file, $destination , $replace);
}
