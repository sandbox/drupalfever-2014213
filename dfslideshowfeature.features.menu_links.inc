<?php
/**
 * @file
 * dfslideshowfeature.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function dfslideshowfeature_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation:node/add/dfslideshow
  $menu_links['navigation:node/add/dfslideshow'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/dfslideshow',
    'router_path' => 'node/add/dfslideshow',
    'link_title' => 'dfSlideshow',
    'options' => array(
      'attributes' => array(
        'title' => 'Images specifically to be displayed on the front page Slideshow banner.',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'node/add',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('dfSlideshow');


  return $menu_links;
}
