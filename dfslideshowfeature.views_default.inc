<?php
/**
 * @file
 * dfslideshowfeature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function dfslideshowfeature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'dfbannerslideshow';
  $view->description = 'This slideshow with be displayed on the front page';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'DF Banner Slideshow';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['style_plugin'] = 'slideshow';
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['slideshow_type'] = 'flexslider_views_slideshow';
  $handler->display->display_options['style_options']['slideshow_skin'] = 'default';
  $handler->display->display_options['style_options']['skin_info'] = array(
    'class' => 'default',
    'name' => 'Default',
    'module' => 'views_slideshow',
    'path' => '',
    'stylesheets' => array(),
  );
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_pager']['weight'] = '1';
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_pager']['type'] = 'views_slideshow_pager_fields';
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_pager']['views_slideshow_pager_fields_fields'] = array(
    'field_dfslideshow_image' => 0,
    'title' => 0,
    'counter' => 0,
  );
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_controls']['enable'] = 1;
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_controls']['weight'] = '1';
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_controls']['hide_on_single_slide'] = 1;
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_controls']['type'] = 'views_slideshow_controls_text';
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_slide_counter']['weight'] = '1';
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_pager']['enable'] = 1;
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_pager']['weight'] = '1';
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_pager']['hide_on_single_slide'] = 1;
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_pager']['type'] = 'views_slideshow_pager_fields';
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_pager']['views_slideshow_pager_fields_fields'] = array(
    'counter' => 'counter',
    'field_dfslideshow_image' => 0,
    'title' => 0,
  );
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_pager']['views_slideshow_pager_fields_hover'] = 1;
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_controls']['weight'] = '1';
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_controls']['hide_on_single_slide'] = 1;
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_controls']['type'] = 'views_slideshow_controls_text';
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_slide_counter']['weight'] = '1';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Slideshow image */
  $handler->display->display_options['fields']['field_dfslideshow_image']['id'] = 'field_dfslideshow_image';
  $handler->display->display_options['fields']['field_dfslideshow_image']['table'] = 'field_data_field_dfslideshow_image';
  $handler->display->display_options['fields']['field_dfslideshow_image']['field'] = 'field_dfslideshow_image';
  $handler->display->display_options['fields']['field_dfslideshow_image']['label'] = '';
  $handler->display->display_options['fields']['field_dfslideshow_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_dfslideshow_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_dfslideshow_image']['settings'] = array(
    'image_style' => 'df-banner-image',
    'image_link' => '',
  );
  /* Field: Content: Banner link */
  $handler->display->display_options['fields']['field_dfbanner_link']['id'] = 'field_dfbanner_link';
  $handler->display->display_options['fields']['field_dfbanner_link']['table'] = 'field_data_field_dfbanner_link';
  $handler->display->display_options['fields']['field_dfbanner_link']['field'] = 'field_dfbanner_link';
  $handler->display->display_options['fields']['field_dfbanner_link']['label'] = '';
  $handler->display->display_options['fields']['field_dfbanner_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_dfbanner_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_dfbanner_link']['click_sort_column'] = 'url';
  /* Field: Content: Website Name */
  $handler->display->display_options['fields']['field_dfslideshow_website_name']['id'] = 'field_dfslideshow_website_name';
  $handler->display->display_options['fields']['field_dfslideshow_website_name']['table'] = 'field_data_field_dfslideshow_website_name';
  $handler->display->display_options['fields']['field_dfslideshow_website_name']['field'] = 'field_dfslideshow_website_name';
  $handler->display->display_options['fields']['field_dfslideshow_website_name']['label'] = '';
  $handler->display->display_options['fields']['field_dfslideshow_website_name']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[field_dfbanner_link]';
  $handler->display->display_options['fields']['title']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['alt'] = '[title]';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['exclude'] = TRUE;
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'dfslideshow' => 'dfslideshow',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['dfbannerslideshow'] = $view;

  return $export;
}
