<?php
/**
 * @file
 * dfslideshowfeature.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dfslideshowfeature_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer dfslideshow feature.
  $permissions['administer dfslideshow feature'] = array(
    'name' => 'administer dfslideshow feature',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'dfslideshowfeature',
  );

  // Exported permission: create dfslideshow content.
  $permissions['create dfslideshow content'] = array(
    'name' => 'create dfslideshow content',
    'roles' => array(
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any dfslideshow content.
  $permissions['delete any dfslideshow content'] = array(
    'name' => 'delete any dfslideshow content',
    'roles' => array(
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own dfslideshow content.
  $permissions['delete own dfslideshow content'] = array(
    'name' => 'delete own dfslideshow content',
    'roles' => array(
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any dfslideshow content.
  $permissions['edit any dfslideshow content'] = array(
    'name' => 'edit any dfslideshow content',
    'roles' => array(
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own dfslideshow content.
  $permissions['edit own dfslideshow content'] = array(
    'name' => 'edit own dfslideshow content',
    'roles' => array(
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
