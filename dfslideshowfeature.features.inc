<?php
/**
 * @file
 * dfslideshowfeature.features.inc
 */

/**
 * Implements hook_views_api().
 */
function dfslideshowfeature_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function dfslideshowfeature_image_default_styles() {
  $styles = array();

  // Exported image style: df-banner-image.
  $styles['df-banner-image'] = array(
    'name' => 'df-banner-image',
    'effects' => array(
      1 => array(
        'label' => 'Javascript crop',
        'help' => 'Create a crop with a javascript toolbox.',
        'effect callback' => 'imagecrop_effect',
        'form callback' => 'imagecrop_effect_form',
        'summary theme' => 'imagecrop_effect_summary',
        'module' => 'imagecrop',
        'name' => 'imagecrop_javascript',
        'data' => array(
          'width' => 950,
          'height' => 350,
          'xoffset' => 'center',
          'yoffset' => 'center',
          'resizable' => 1,
          'downscaling' => 0,
          'aspect_ratio' => 'KEEP',
          'disable_if_no_data' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: dfslideshow_overlay.
  $styles['dfslideshow_overlay'] = array(
    'name' => 'dfslideshow_overlay',
    'effects' => array(
      5 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 950,
          'height' => 350,
        ),
        'weight' => -10,
      ),
      2 => array(
        'label' => 'Overlay (watermark)',
        'help' => 'Choose the file image you wish to use as an overlay, and position it in a layer on top of the canvas.',
        'effect callback' => 'canvasactions_file2canvas_effect',
        'dimensions passthrough' => TRUE,
        'form callback' => 'canvasactions_file2canvas_form',
        'summary theme' => 'canvasactions_file2canvas_summary',
        'module' => 'imagecache_canvasactions',
        'name' => 'canvasactions_file2canvas',
        'data' => array(
          'xpos' => 0,
          'ypos' => 0,
          'alpha' => 100,
          'path' => 'public://dfslideshowfeature_images/banner-overlay.png',
        ),
        'weight' => -9,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function dfslideshowfeature_node_info() {
  $items = array(
    'dfslideshow' => array(
      'name' => t('dfSlideshow'),
      'base' => 'node_content',
      'description' => t('Images specifically to be displayed on the front page Slideshow banner.'),
      'has_title' => '1',
      'title_label' => t('Image name'),
      'help' => '',
    ),
  );
  return $items;
}
